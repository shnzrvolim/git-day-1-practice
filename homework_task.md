### Task #1

In this task you need to:

- Create a public repository on GitLab (name it git-day-1-practice), check readme file while creating.
- Clone the repository using `https`

### Task #2

In this task you need to:

- Create `index.html` file
- Add the following text to it

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>The main document</title>
</head>
<body>
  <h1>Hello World!</h1>
</body>
</html>
```

- Publish your changes to GitLab

### Task #3

In this task you need to:

- Update index.html with following content

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>The main document 2</title>
</head>
<body>
  <h1>Hello World!</h1>
</body>
</html>
```

- Cancel this change using git command (try git reset, than git restore and see result)

### Task #4

In this task you need to:

- Add the following text to it

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>The main document</title>
</head>
<body>
  <h1>Hello World!</h1>
  <p>
    Some text
  </p>
</body>
</html>
```

- Index the changes (add to the staging area)

- Make a commit
- Change previous commit (use --amend) (update message also)
- Check the log
- Publish to the GitLab